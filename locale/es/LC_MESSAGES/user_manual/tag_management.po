# Spanish translations for docs_krita_org_user_manual___tag_management.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___tag_management\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-24 18:57+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../user_manual/tag_management.rst:1
msgid "Detailed steps on how to use the tags to organize resources in Krita."
msgstr ""

#: ../../user_manual/tag_management.rst:11
msgid "Tags"
msgstr ""

#: ../../user_manual/tag_management.rst:16
msgid "Tag Management"
msgstr "Gestión de etiquetas"

#: ../../user_manual/tag_management.rst:18
msgid ""
"Tags are how you organize common types of resources. They can be used with "
"brushes, gradients, patterns, and even brush tips. You can select them from "
"a drop-down menu above the resources. Selecting a tag will filter all the "
"resources by that tag. Selecting the tag of :guilabel:`All` will show all "
"resources.  Krita comes installed with a few default tags. You can create "
"and edit your own as well. The tags are managed similarly across the "
"different types of resources."
msgstr ""

#: ../../user_manual/tag_management.rst:20
msgid ""
"You can use tags together with the :ref:`Pop-up Palette <navigation>` for "
"increased productivity."
msgstr ""

#: ../../user_manual/tag_management.rst:23
msgid ".. image:: images/resources/Tag_Management.jpeg"
msgstr ""

#: ../../user_manual/tag_management.rst:25
msgid ""
"You can select different brush tags in the pop-up palette. This can be a "
"quick way to access your favorite brushes."
msgstr ""

#: ../../user_manual/tag_management.rst:28
msgid "Adding a New Tag for a Brush"
msgstr ""

#: ../../user_manual/tag_management.rst:30
msgid ""
"By pressing the :guilabel:`+` next to the tag selection, you will get an "
"option to add a tag. Type in the name you want and press the :kbd:`Enter` "
"key. You will need to go back to the :guilabel:`All` tag to start assigning "
"brushes."
msgstr ""

#: ../../user_manual/tag_management.rst:33
msgid "Assigning an Existing Tag to a Brush"
msgstr ""

#: ../../user_manual/tag_management.rst:35
msgid ""
"Right-click on a brush in the Brush Presets Docker. You will get an option "
"to assign a tag to the brush."
msgstr ""

#: ../../user_manual/tag_management.rst:38
msgid "Changing a Tag's Name"
msgstr "Cambiar el nombre de una etiqueta"

#: ../../user_manual/tag_management.rst:40
msgid ""
"Select the existing tag that you want to have changed from the drop-down. "
"Press the :guilabel:`+` icon next to the tag. You will get an option to "
"rename it. Press the :kbd:`Enter` key to confirm. All the existing brushes "
"will remain in the newly named tag."
msgstr ""

#: ../../user_manual/tag_management.rst:43
msgid "Deleting a Tag"
msgstr "Borrar una etiqueta"

#: ../../user_manual/tag_management.rst:44
msgid ""
"Select the existing tag that you want to have removed from the drop-down. "
"Press the :guilabel:`+` icon next to the tag. You will get an option to "
"remove it."
msgstr ""

#: ../../user_manual/tag_management.rst:47
msgid ""
"The default brushes that come with Krita cannot have their default tags "
"removed."
msgstr ""
