# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-01 22:02+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:62
msgid ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"
msgstr ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: Smart fläckverktyg"

#: ../../reference_manual/tools/smart_patch.rst:1
msgid "Krita's smart patch tool reference."
msgstr "Referens för Kritas smarta fläckverktyg."

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Smart Patch"
msgstr "Smart fläck"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Automatic Healing"
msgstr "Automatisk reparation"

#: ../../reference_manual/tools/smart_patch.rst:16
msgid "Smart Patch Tool"
msgstr "Smart fläckverktyg"

#: ../../reference_manual/tools/smart_patch.rst:18
msgid "|toolsmartpatch|"
msgstr "|toolsmartpatch|"

#: ../../reference_manual/tools/smart_patch.rst:20
msgid ""
"The smart patch tool allows you to seamlessly remove elements from the "
"image. It does this by letting you draw the area which has the element you "
"wish to remove, and then it will attempt to use patterns already existing in "
"the image to fill the blank."
msgstr ""
"Det smarta fläckverktyget låter dig sömlöst ta bort element från bilden. Det "
"gör det genom att låta dig rita området som har elementet du vill ska tas "
"bort, och försöker därefter använda redan befintliga mönster på bilden för "
"att fylla i tomrummet."

#: ../../reference_manual/tools/smart_patch.rst:22
msgid "You can see it as a smarter version of the clone brush."
msgstr "Det kan betraktas som en smartare version av dupliceringspenseln."

#: ../../reference_manual/tools/smart_patch.rst:25
msgid ".. image:: images/tools/Smart-patch.gif"
msgstr ".. image:: images/tools/Smart-patch.gif"

#: ../../reference_manual/tools/smart_patch.rst:26
msgid "The smart patch tool has the following tool options:"
msgstr "Det smarta fläckverktyget har följande verktygsalternativ:"

#: ../../reference_manual/tools/smart_patch.rst:29
msgid "Accuracy"
msgstr "Noggrannhet"

#: ../../reference_manual/tools/smart_patch.rst:31
msgid ""
"Accuracy indicates how many samples, and thus how often the algorithm is "
"run. A low accuracy will do few samples, but will also run the algorithm "
"fewer times, making it faster. Higher accuracy will do many samples, making "
"the algorithm run more often and give more precise results, but because it "
"has to do more work, it is slower."
msgstr ""
"Noggrannhet anger hur många samplingar, och sålunda hur ofta algoritmen "
"körs. En låg noggrannhet utför få samplingar, men kör också algoritmen färre "
"gånger, vilket gör den snabbare. Högre noggrannhet utför många samplingar, "
"vilket gör att algoritmen körs mycket oftare och ger noggrannare resultat, "
"men eftersom den måste utföra mycket mer arbete, är den långsammare."

#: ../../reference_manual/tools/smart_patch.rst:34
msgid "Patch size"
msgstr "Fläckstorlek"

#: ../../reference_manual/tools/smart_patch.rst:36
msgid ""
"Patch size determines how big the size of the pattern to choose is. This "
"will be best explained with some testing, but if the surrounding image has "
"mostly small elements, like branches, a small patch size will give better "
"results, while a big patch size will be better for images with big elements, "
"so they get reused as a whole."
msgstr ""
"Fläckstorlek bestämmer hur stor mönsterstorleken att välja ska vara. Det "
"förklaras bäst genom att prova, men om den omgivande bilden i huvudsak har "
"små element, som grenar, ger en liten fläckstorlek bättre resultat, medan en "
"stor fläckstorlek är bättre med stora element, så att de återanvänds i sin "
"helhet."
