msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 18:04+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita menuselection\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/layers_and_masks/file_layers.rst:1
msgid "How to use file layers in Krita."
msgstr "Como usar as camadas de ficheiros no Krita."

#: ../../reference_manual/layers_and_masks/file_layers.rst:12
#: ../../reference_manual/layers_and_masks/file_layers.rst:17
msgid "File Layers"
msgstr "Camadas de Ficheiros"

#: ../../reference_manual/layers_and_masks/file_layers.rst:12
msgid "Layers"
msgstr "Camadas"

#: ../../reference_manual/layers_and_masks/file_layers.rst:12
msgid "External File"
msgstr "Ficheiro Externo"

#: ../../reference_manual/layers_and_masks/file_layers.rst:19
msgid ""
"File Layers are references to files outside of the document: If the "
"referenced document updates, the file layer will update. Do not remove the "
"original file on your computer once you add it to Krita. Deleting your "
"original image will break the file layer. If Krita cannot find the original "
"file, it'll ask you where to find it. File layers cannot display animations."
msgstr ""
"As Camadas de Ficheiros são referências para ficheiros fora do documento: Se "
"o documento referenciado for actualizado, a camada de ficheiro também o "
"será. Não remova o ficheiro original no seu computador após o ter adicionado "
"ao Krita. Se apagar a sua imagem original, irá danificar a camada do "
"ficheiro. Se o Krita não conseguir encontrar o ficheiro original, perguntar-"
"lhe-á onde o deseja encontrar. As camadas de ficheiros não podem apresentar "
"animações."

#: ../../reference_manual/layers_and_masks/file_layers.rst:21
msgid "File Layers have the following scaling options:"
msgstr "As Camadas de Ficheiros têm as seguintes opções de escala:"

#: ../../reference_manual/layers_and_masks/file_layers.rst:23
msgid "No Scaling"
msgstr "Sem Escala"

#: ../../reference_manual/layers_and_masks/file_layers.rst:24
msgid "This'll import the file layer with the full pixel-size."
msgstr ""
"Isto irá importar a camada do ficheiro com o tamanho completo dos pixels."

#: ../../reference_manual/layers_and_masks/file_layers.rst:25
msgid "Scale to Image Size"
msgstr "Escalar ao Tamanho da Imagem"

#: ../../reference_manual/layers_and_masks/file_layers.rst:26
msgid ""
"Scales the file layer to fit exactly within the canvas boundaries of the "
"image."
msgstr ""
"Ajusta a escala da camada de ficheiro para caber exactamente dentro dos "
"limites da área de desenho da imagem."

#: ../../reference_manual/layers_and_masks/file_layers.rst:28
msgid "Adapt to image resolution"
msgstr "Adaptar à resolução da imagem"

#: ../../reference_manual/layers_and_masks/file_layers.rst:28
msgid ""
"If the imported layer and the image have a different resolution, it'll scale "
"the filelayer by scaling its resolution. In other words, import a 600dpi A4 "
"image onto a 300dpi A4 image, and the filelayer will be scaled to fit "
"precisely on the 300dpi image. Useful for comics, where the ink-layer is "
"preferred to be at a higher resolution than the colors."
msgstr ""
"Se a camada importada e a imagem tiverem uma resolução diferente, irá "
"ajustar a escala da camada de ficheiro através do ajuste da escala da sua "
"resolução. Por outras palavras, se importar uma imagem A4 a 600ppp sobre uma "
"imagem A4 a 300ppp, a escala da camada de ficheiro será ajustada para caber "
"de forma precisa na imagem a 300ppp. É útil para as bandas desenhadas, onde "
"a camada de pintura convém estar numa resolução mais elevada que as cores."

#: ../../reference_manual/layers_and_masks/file_layers.rst:30
msgid ""
"File Layers can currently not be painted on. If you want to transform a file "
"layer, you need to apply a transformation mask to it and use that."
msgstr ""
"Não é possível pintar sobre as Camadas de Ficheiros File. Se quiser "
"transformar uma camada de ficheiro, terá de aplicar uma máscara de "
"transformação sobre ela e usá-la."

#: ../../reference_manual/layers_and_masks/file_layers.rst:34
msgid ""
"In the layerdocker, next to the file layer only, there's a little folder "
"icon. Pressing that will open the file pointed at in Krita if it hadn't yet. "
"Using the properties you can make the file layer point to a different file."
msgstr ""
"Na área de camadas, apenas a seguir à camada de ficheiro, existe um pequeno "
"ícone com uma pasta. Se carregar no mesmo, irá abrir o ficheiro referenciado "
"no Krita, caso ainda não o tenha feito anteriormente. Ao usar as "
"propriedades, poderá fazer com que a camada do ficheiro aponte para um "
"ficheiro diferente."

#: ../../reference_manual/layers_and_masks/file_layers.rst:38
msgid ""
"You can turn any set of layers into a file layer by right-clicking them and "
"doing :menuselection:`Convert --> to File Layer`. It will then open a save "
"prompt for the file location and when done will save the file and replace "
"the layer with a file layer pointing at that file."
msgstr ""
"Poderá transformar qualquer conjunto de camadas numa camada de ficheiro se "
"carregar com o botão direito sobre elas e escolher :menuselection:`Converter "
"--> Para Camada de Ficheiro`. A mesma irá abrir uma mensagem de gravação "
"para a localização do ficheiro e, quando terminar, irá gravar o mesmo e "
"substituir a camada por uma camada de ficheiro que aponta para esse ficheiro."
