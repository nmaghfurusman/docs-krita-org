# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:11+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: images Krita image welcomescreen guilabel\n"

#: ../../reference_manual/welcome_screen.rst:None
msgid ".. image:: images/welcome_screen.png"
msgstr ".. image:: images/welcome_screen.png"

#: ../../reference_manual/welcome_screen.rst:1
msgid "The welcome screen in Krita."
msgstr "O ecrã de boas-vindas no Krita."

#: ../../reference_manual/welcome_screen.rst:10
#: ../../reference_manual/welcome_screen.rst:14
msgid "Welcome Screen"
msgstr "Ecrã de Boas-Vindas"

#: ../../reference_manual/welcome_screen.rst:16
msgid ""
"When you open Krita, starting from version 4.1.3, you will be greeted by a "
"welcome screen. This screen makes it easy for you to get started with Krita, "
"as it provides a collection of shortcuts for the most common tasks that you "
"will probably be doing when you open Krita."
msgstr ""
"Quando abrir o Krita, a partir da versão 4.1.3, ser-lhe-á apresentado um "
"ecrã de boas-vindas. Este ecrã facilita-lhe a introdução ao Krita, dado que "
"oferece uma colecção de atalhos para as tarefas mais comuns que "
"provavelmente iria fazer quando abrisse o Krita."

#: ../../reference_manual/welcome_screen.rst:23
msgid "The screen is divided into 4 sections:"
msgstr "O ecrã está dividido em 4 secções:"

#: ../../reference_manual/welcome_screen.rst:25
msgid ""
"The :guilabel:`Start` section there are links to create new document as well "
"to open an existing document."
msgstr ""
"A secção :guilabel:`Início` possui ligações para criar um novo documento, "
"assim como para abrir um documento existente."

#: ../../reference_manual/welcome_screen.rst:28
msgid ""
"The :guilabel:`Recent Documents` section has a list of recently opened "
"documents from your previous sessions in Krita."
msgstr ""
"A secção :guilabel:`Documentos Recentes` tem uma lista de documentos abertos "
"recentemente nas suas sessões anteriores do Krita."

#: ../../reference_manual/welcome_screen.rst:31
msgid ""
"The :guilabel:`Community` section provides some links to get help, "
"Supporting development of Krita, Source code of Krita and to links to "
"interact with our user community."
msgstr ""
"A secção da :guilabel:`Comunidade` oferece algumas referências para obter "
"ajuda, ajudar no desenvolvimento do Krita, para o código-fonte do Krita e "
"ligações para interagir com a nossa comunidade de utilizadores."

#: ../../reference_manual/welcome_screen.rst:35
msgid ""
"The :guilabel:`News` section, which is disabled by default, when enabled "
"provides you with latest news feeds fetched from Krita website, this will "
"help you stay up to date with the release news and other events happening in "
"our community."
msgstr ""
"A secção de :guilabel:`Notícias`, que se encontra desactivada por omissão, "
"quando está activa oferece-lhe as últimas notícias obtidas a partir da "
"página Web do Krita; isto ajudá-lo-á a manter-se actualizado com as notícias "
"de lançamentos e com outros eventos que acontecem na nossa comunidade."

#: ../../reference_manual/welcome_screen.rst:39
msgid ""
"Other than the above sections the welcome screen also acts as a drop area "
"for opening any document. You just have to drag and drop a Krita document or "
"any supported image files on the empty area around the sections to open it "
"in Krita."
msgstr ""
"Para além das secções acima, o ecrã de boas-vindas também actua como uma "
"área de largada para abrir qualquer documento. Apenas terá de arrastar e "
"largar um documento do Krita ou qualquer outro formato de imagem suportado "
"na área vazia em contorno das secções para os abrir no Krita."
