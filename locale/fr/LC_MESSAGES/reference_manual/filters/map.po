msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:46+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/filters/map.rst:1
msgid "Overview of the map filters."
msgstr ""

#: ../../reference_manual/filters/map.rst:11
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/map.rst:16
msgid "Map"
msgstr "Carte"

#: ../../reference_manual/filters/map.rst:18
msgid "Filters that are signified by them mapping the input image."
msgstr ""

#: ../../reference_manual/filters/map.rst:20
#: ../../reference_manual/filters/map.rst:23
msgid "Small Tiles"
msgstr "Petites tuiles"

#: ../../reference_manual/filters/map.rst:20
#, fuzzy
#| msgid "Small Tiles"
msgid "Tiles"
msgstr "Petites tuiles"

#: ../../reference_manual/filters/map.rst:25
msgid "Tiles the input image, using its own layer as output."
msgstr ""

#: ../../reference_manual/filters/map.rst:27
msgid "Height Map"
msgstr ""

#: ../../reference_manual/filters/map.rst:27
#, fuzzy
#| msgid "Phong Bumpmap"
msgid "Bumpmap"
msgstr "Modèle de Phong"

#: ../../reference_manual/filters/map.rst:27
#, fuzzy
#| msgid "Normalize"
msgid "Normal Map"
msgstr "Normaliser"

#: ../../reference_manual/filters/map.rst:30
msgid "Phong Bumpmap"
msgstr "Modèle de Phong"

#: ../../reference_manual/filters/map.rst:33
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ""

#: ../../reference_manual/filters/map.rst:34
msgid ""
"Uses the input image as a height-map to output a 3d something, using the "
"phong-lambert shading model. Useful for checking one's height maps during "
"game texturing. Checking the :guilabel:`Normal Map` box will make it use all "
"channels and interpret them as a normal map."
msgstr ""

#: ../../reference_manual/filters/map.rst:37
msgid "Round Corners"
msgstr "Coins arrondis"

#: ../../reference_manual/filters/map.rst:39
msgid "Adds little corners to the input image."
msgstr ""

#: ../../reference_manual/filters/map.rst:41
#: ../../reference_manual/filters/map.rst:44
msgid "Normalize"
msgstr "Normaliser"

#: ../../reference_manual/filters/map.rst:46
msgid ""
"This filter takes the input pixels, puts them into a 3d vector, and then "
"normalizes (makes the vector size exactly 1) the values. This is helpful for "
"normal maps and some minor image-editing functions."
msgstr ""

#: ../../reference_manual/filters/map.rst:48
#: ../../reference_manual/filters/map.rst:51
msgid "Gradient Map"
msgstr "Dégradé"

#: ../../reference_manual/filters/map.rst:48
#, fuzzy
#| msgid "Gradient Map"
msgid "Gradient"
msgstr "Dégradé"

#: ../../reference_manual/filters/map.rst:54
msgid ".. image:: images/filters/Krita_filter_gradient_map.png"
msgstr ""

#: ../../reference_manual/filters/map.rst:55
msgid ""
"Maps the lightness of the input to the selected gradient. Useful for fancy "
"artistic effects."
msgstr ""

#: ../../reference_manual/filters/map.rst:57
msgid ""
"In 3.x you could only select predefined gradients. In 4.0, you can select "
"gradients and change them on the fly, as well as use the gradient map filter "
"as a filter layer or filter brush."
msgstr ""
