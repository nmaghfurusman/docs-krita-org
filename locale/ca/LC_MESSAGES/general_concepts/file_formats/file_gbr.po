# Translation of docs_krita_org_general_concepts___file_formats___file_gbr.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:40+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_gbr.rst:1
msgid "The Gimp Brush file format as used in Krita."
msgstr ""
"El format de fitxer de Pinzell del Gimp tal com s'utilitza en el Krita."

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "Gimp Brush"
msgstr "Pinzell del Gimp"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "GBR"
msgstr "GBR"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "*.gbr"
msgstr "*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:15
msgid "\\*.gbr"
msgstr "\\*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:17
msgid ""
"The GIMP brush format. Krita can open, save and use these files as :ref:"
"`predefined brushes <predefined_brush_tip>`."
msgstr ""
"El format de pinzell del GIMP. El Krita pot obrir, desar i utilitzar aquests "
"fitxers com a :ref:`pinzells predefinits <predefined_brush_tip>`."

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_gbr.rst:19
msgid ""
"There's three things that you can decide upon when exporting a ``*.gbr``:"
msgstr "Hi ha tres coses que podeu decidir en exportar un ``*.gbr``:"

#: ../../general_concepts/file_formats/file_gbr.rst:21
msgid "Name"
msgstr "Nom"

#: ../../general_concepts/file_formats/file_gbr.rst:22
msgid ""
"This name is different from the file name, and will be shown inside Krita as "
"the name of the brush."
msgstr ""
"Aquest nom és diferent del nom del fitxer i es mostrarà dins del Krita com a "
"nom del pinzell."

#: ../../general_concepts/file_formats/file_gbr.rst:23
msgid "Spacing"
msgstr "Espaiat"

#: ../../general_concepts/file_formats/file_gbr.rst:24
msgid "This sets the default spacing."
msgstr "Estableix l'espaiat predeterminat."

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid "Use color as mask"
msgstr "Usa el color com a màscara"

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""
"Convertirà els valors més foscos de la imatge com els que pinten i els més "
"blancs com a transparents. Desmarqueu aquesta opció si utilitzeu imatges en "
"color per al pinzell."

#: ../../general_concepts/file_formats/file_gbr.rst:28
msgid ""
"GBR brushes are otherwise unremarkable, and limited to 8bit color precision."
msgstr ""
"D'altra banda, els pinzells GBR són irrellevants i estan limitats a una "
"precisió de color de 8 bits."
