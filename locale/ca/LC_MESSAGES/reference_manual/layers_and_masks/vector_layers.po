# Translation of docs_krita_org_reference_manual___layers_and_masks___vector_layers.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-25 13:51+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
msgid ".. image:: images/vector/Fill_rule_even-odd.svg"
msgstr ".. image:: images/vector/Fill_rule_even-odd.svg"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
msgid ".. image:: images/vector/Fill_rule_non-zero.svg"
msgstr ".. image:: images/vector/Fill_rule_non-zero.svg"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:1
msgid "How to use vector layers in Krita."
msgstr "Com emprar les capes vectorials en el Krita."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
msgid "Vector"
msgstr "Vectors"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:20
msgid "Vector Layers"
msgstr "Capes vectorials"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:24
msgid ""
"This page is outdated. Check :ref:`vector_graphics` for a better overview."
msgstr ""
"Aquesta pàgina està desactualitzada. Comproveu els :ref:`vector_graphics` "
"del manual d'usuari per obtenir una millor visió general."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:27
msgid "What is a Vector Layer?"
msgstr "Què és una capa vectorial?"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:29
msgid ""
"A Vector Layers, also known as a shape layer, is a type of layers that "
"contains only vector elements."
msgstr ""
"Les capes vectorials, també conegudes com a capes de forma, són un tipus de "
"capes que només contenen elements vectorials."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:31
msgid ""
"This is how vector layers will appear in the :program:`Krita` Layers docker."
msgstr ""
"Així és com apareixeran les capes vectorials a l'acoblador Capes del :"
"program:`Krita`."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:34
msgid ".. image:: images/vector/Vectorlayer.png"
msgstr ".. image:: images/vector/Vectorlayer.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:35
msgid ""
"It shows the vector contents of the layer on the left side. The icon showing "
"the page with the red bookmark denotes that it is a vector layer. To the "
"right of that is the layer name. Next are the layer visibility and "
"accessibility icons. Clicking the \"eye\" will toggle visibility. Clicking "
"the lock into a closed position will lock the content and editing will no "
"longer be allowed until it is clicked again and the lock on the layer is "
"released."
msgstr ""
"Mostra els continguts vectorials de la capa al costat esquerre. La icona que "
"mostra la pàgina amb el marcador vermell denota que és una capa vectorial. A "
"la dreta d'això hi ha el nom de la capa. A continuació es mostren les icones "
"de visibilitat i accessibilitat de la capa. En fer clic dret a l'«ull» es "
"canviarà la visibilitat. En fer clic dret al bloqueig en una posició tancada "
"es bloquejarà el contingut i l'edició ja no es permetrà fins que es torni a "
"fer clic dret i s'alliberi el bloqueig de la capa."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:38
msgid "Creating a vector layer"
msgstr "Crear una capa vectorial"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:40
msgid ""
"You can create a vector layer in two ways. Using the extra options from the "
"\"Add Layer\" button you can click the \"Vector Layer\" item and it will "
"create a new vector layer. You can also drag a rectangle or ellipse from the "
"**Add shape** dock onto an active Paint Layer.  If the active layer is a "
"Vector Layer then the shape will be added directly to it."
msgstr ""
"Podeu crear una capa vectorial de dues maneres. Utilitzant les opcions "
"addicionals del botó «Afegeix una capa», podeu fer clic a l'element «Capa "
"vectorial» i es crearà una capa vectorial nova. També podreu arrossegar un "
"rectangle o una el·lipse des de l'acoblador **Afegeix una forma** a dins "
"d'una Capa de pintura activa. Si la capa activa és una Capa vectorial, "
"llavors la forma s'hi afegirà directament."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:43
msgid "Editing Shapes on a Vector Layer"
msgstr "Editar les formes sobre una capa vectorial"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:47
msgid ""
"There's currently a bug with the vector layers that they will always "
"consider themselves to be at 72dpi, regardless of the actual pixel-size. "
"This can make manipulating shapes a little difficult, as the precise input "
"will not allow cm or inch, even though the vector layer coordinate system "
"uses those as a basis."
msgstr ""
"Actualment hi ha un error amb les capes vectorials que sempre es "
"consideraran a 72 ppp, independentment de la mida real dels píxels. Això pot "
"dificultar una mica la manipulació de les formes, ja que l'entrada precisa "
"no permetrà centímetres o polzades, fins i tot encara que el sistema de "
"coordenades de la capa vectorial les utilitzi com a base."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:50
msgid "Basic Shape Manipulation"
msgstr "Manipulació bàsica de la forma"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:52
msgid ""
"To edit the shape and colors of your vector element, you will need to use "
"the basic shape manipulation tool."
msgstr ""
"Per editar la forma i els colors del vostre element vectorial, podreu "
"utilitzar l'eina bàsica per a la manipulació de les formes."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:54
msgid ""
"Once you have selected this tool, click on the element you want to "
"manipulate and you will see guides appear around your shape."
msgstr ""
"Un cop hàgiu seleccionat aquesta eina, feu clic sobre l'element que voleu "
"manipular i veureu aparèixer guies al voltant de la vostra forma."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:57
msgid ".. image:: images/vector/Vectorguides.png"
msgstr ".. image:: images/vector/Vectorguides.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:58
msgid ""
"There are four ways to manipulate your image using this tool and the guides "
"on your shape."
msgstr ""
"Hi ha quatre maneres de manipular la vostra imatge utilitzant aquesta eina i "
"les guies de la vostra forma."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:61
msgid "Transform/Move"
msgstr "Transformar/Moure"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:64
msgid ".. image:: images/vector/Transform.png"
msgstr ".. image:: images/vector/Transform.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:65
msgid ""
"This feature of the tool allows you to move your object by clicking and "
"dragging your shape around the canvas. Holding the :kbd:`Ctrl` key will lock "
"your moves to one axis."
msgstr ""
"Aquesta característica de l'eina permet moure l'objecte fent clic i "
"arrossegant la vostra forma al voltant del llenç. Mantenint premuda la "
"tecla :kbd:`Ctrl` bloquejarà els vostres moviments a un eix."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:68
msgid "Size/Stretch"
msgstr "Mida/Estirar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:71
msgid ".. image:: images/vector/Resize.png"
msgstr ".. image:: images/vector/Resize.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:72
msgid ""
"This feature of the tool allows you to stretch your shape.  Selecting a "
"midpoint will allow stretching along one axis. Selecting a corner point will "
"allow stretching across both axis. Holding the :kbd:`Shift` key will allow "
"you to scale your object. Holding the :kbd:`Ctrl` key will cause your "
"manipulation to be mirrored across your object."
msgstr ""
"Aquesta característica de l'eina permet estirar la vostra forma. Seleccionar "
"un punt mig permetrà estirar al llarg d'un eix. Seleccionar un punt de "
"cantonada permetrà estirar a través dels dos eixos. Mantenint premuda la "
"tecla :kbd:`Majús.` podreu escalar l'objecte. Mantenint premuda la tecla :"
"kbd:`Ctrl` fareu que la vostra manipulació es reflecteixi en l'objecte."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:75
msgid "Rotate"
msgstr "Girar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:78
msgid ".. image:: images/vector/Rotatevector.png"
msgstr ".. image:: images/vector/Rotatevector.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:79
msgid ""
"This feature of the tool will allow you to rotate your object around its "
"center. Holding the :kbd:`Ctrl` key will cause your rotation to lock to 45 "
"degree angles."
msgstr ""
"Aquesta característica de l'eina permetrà girar l'objecte al voltant del seu "
"centre. Mantenint premuda la tecla :kbd:`Ctrl` el gir es bloquejarà en "
"angles de 45 graus."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:82
msgid "Skew"
msgstr "Inclinar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:85
msgid ".. image:: images/vector/Skew.png"
msgstr ".. image:: images/vector/Skew.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:86
msgid "This feature of the tool will allow you to skew your object."
msgstr "Aquesta característica de l'eina permetrà inclinar l'objecte."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:90
msgid ""
"At the moment there is no way to scale only one side of your vector object. "
"The developers are aware that this could be useful and will work on it as "
"manpower allows."
msgstr ""
"En aquest moment no hi ha manera d'escalar només un costat de l'objecte "
"vectorial. Els desenvolupadors són conscients que això podria ser útil i hi "
"treballaran a mesura que la mà d'obra ho permeti."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:93
msgid "Point and Curve Shape Manipulation"
msgstr "Manipulació de les formes amb punts i corbes"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:95
msgid ""
"Double-click on a vector object to edit the specific points or curves which "
"make up the shape. Click and drag a point to move it around the canvas. "
"Click and drag along a line to curve it between two points. Holding the :kbd:"
"`Ctrl` key will lock your moves to one axis."
msgstr ""
"Feu doble clic sobre un objecte vectorial per editar els punts o corbes "
"específics que conformen la forma. Feu clic i arrossegueu un punt per a "
"moure'l al voltant del llenç. Feu clic i arrossegueu al llarg d'una línia "
"per a corbar-la entre dos punts. Mantenint premuda la tecla :kbd:`Ctrl` es "
"bloquejaran els vostres moviments a un eix."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:98
msgid ".. image:: images/vector/Pointcurvemanip.png"
msgstr ".. image:: images/vector/Pointcurvemanip.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:100
msgid "Stroke and Fill"
msgstr "Traçar i emplenar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:102
msgid ""
"In addition to being defined by points and curves, a shape also has two "
"defining properties: **Fill** and **Stroke**. **Fill** defines the color, "
"gradient, or pattern that fills the space inside of the shape object. "
"'**Stroke**' defines the color, gradient, pattern, and thickness of the "
"border along the edge of the shape. These two can be edited using the "
"**Stroke and Fill** dock. The dock has two modes. One for stroke and one for "
"fill. You can change modes by clicking in the dock on the filled square or "
"the black line. The active mode will be shown by which is on top of the "
"other."
msgstr ""
"A més d'estar definida per punts i corbes, una forma també té dues "
"propietats definitòries: **Emplenament** i **Traç**. L'**emplenament** "
"definirà el color, degradat o patró que emplenarà l'espai dins de l'objecte "
"de la forma. El **traç** definirà el color, degradat, patró i gruix de la "
"vora al llarg de la vora de la forma. Aquests dos podran editar-se "
"utilitzant l'acoblador **Traça i emplena**. L'acoblador té dos modes. Un per "
"al traç i un altra per a l'emplenament. Podreu canviar els modes fent clic a "
"l'acoblador sobre el quadrat d'emplenament o a la línia negra. El mode actiu "
"es mostrarà per qual està a sobre de l'altre."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:104
msgid ""
"Here is the dock with the fill element active. Notice the red line across "
"the solid white square. This tells us that there is no fill assigned "
"therefore the inside of the shape will be transparent."
msgstr ""
"Aquí hi ha l'acoblador amb l'element d'emplenament actiu. Observeu la línia "
"vermella a través del quadrat blanc sòlid. Això ens indica que no hi ha "
"emplenat assignat, per tant, l'interior de la forma serà transparent."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:107
#: ../../reference_manual/layers_and_masks/vector_layers.rst:129
msgid ".. image:: images/vector/Strokeandfill.png"
msgstr ".. image:: images/vector/Strokeandfill.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:108
msgid "Here is the dock with the stroke element active."
msgstr "Aquí hi ha l'acoblador amb l'element del traç actiu."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:111
msgid ".. image:: images/vector/Strokeandfillstroke.png"
msgstr ".. image:: images/vector/Strokeandfillstroke.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:113
msgid "Editing Stroke Properties"
msgstr "Editar les propietats del traç"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:115
msgid ""
"The stroke properties dock will allow you to edit a different aspect of how "
"the outline of your vector shape looks."
msgstr ""
"L'acoblador per a les propietats del traç permetrà editar un aspecte "
"diferent de com es veurà el contorn de la vostra forma vectorial."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:118
msgid ".. image:: images/vector/Strokeprops.png"
msgstr ".. image:: images/vector/Strokeprops.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:119
msgid ""
"The style selector allows you to choose different patterns and line styles. "
"The width option changes the thickness of the outline on your vector shape. "
"The cap option changes how line endings appear. The join option changes how "
"corners appear."
msgstr ""
"El selector de l'estil permet escollir diferents patrons i estils de línia. "
"L'opció d'amplada canviarà el gruix del contorn a la vostra forma vectorial. "
"L'opció de punta canviarà la manera en què apareixen els finals de línia. "
"L'opció d'unir canviarà la manera en què apareixen les cantonades."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:121
msgid ""
"The Miter limit controls how harsh the corners of your object will display. "
"The higher the number the more the corners will be allowed to stretch out "
"past the points. Lower numbers will restrict the stroke to shorter and less "
"sharp corners."
msgstr ""
"El Límit del biaix controlarà quant d'aspres es mostraran les cantonades de "
"l'objecte. Com més alt sigui el número, més es podran estirar les cantonades "
"més enllà dels punts. Els números més baixos restringiran el traç a "
"cantonades més curtes i menys afilades."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:124
msgid "Editing Fill Properties"
msgstr "Editar les propietats de l'emplenat"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:126
msgid ""
"All of the fill properties are contained in the **Stroke and Fill** dock."
msgstr ""
"Totes les propietats de l'emplenat estan contingudes a l'acoblador **Traça i "
"emplena**."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:130
msgid ""
"The large red **X** button will set the fill to none causing the area inside "
"of the vector shape to be transparent."
msgstr ""
"El botó gran de **X** vermella establirà l'emplenat a cap, el qual farà que "
"l'àrea dins de la forma vectorial sigui transparent."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:132
msgid ""
"To the right of that is the solid square. This sets the fill to be a solid "
"color which is displayed in the long button and can be selected by pressing "
"the arrow just to the right of the long button. To the right of the solid "
"square is the gradient button. This will set the fill to display as a "
"gradient. A gradient can be selected by pressing the down arrow next to the "
"long button."
msgstr ""
"A la dreta d'aquest hi ha el quadrat sòlid. Estableix que l'emplenat sigui "
"un color sòlid que es mostra en el botó llarg i es pot seleccionar prement "
"la fletxa que hi ha just a la dreta del botó llarg. A la dreta del quadrat "
"sòlid hi ha el botó de degradat. Aquest establirà l'emplenat per a mostrar-"
"lo com un degradat. Es pot seleccionar un degradat prement la fletxa cap "
"avall que hi ha al costat del botó llarg."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:134
msgid ""
"Under the **X** is a button that shows a pattern. This inside area will be "
"filled with a pattern. A pattern can be chosen by pressing the arrows next "
"to the long button. The two other buttons are for **fill rules**: the way a "
"self-overlapping path is filled."
msgstr ""
"A sota de la **X** hi ha un botó que mostra un patró. Aquesta zona interior "
"s'emplenarà amb un patró. Es pot triar un patró prement les fletxes que hi "
"ha al costat del botó llarg. Els altres dos botons són per a les regles de "
"l'emplenat: la manera en què s'emplena un camí que se superposa."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:136
msgid ""
"The button with the inner square blank toggles even-odd mode, where every "
"filled region of the path is next to an unfilled one, like this:"
msgstr ""
"El botó amb el blanc quadrat interior canvia el mode parell i imparell, on "
"cada regió emplenada del camí es troba al costat d'una sense emplenar, "
"d'aquesta manera:"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:142
msgid ""
"The button with the inner square filled toggles non zero mode, where most of "
"the time a self overlapping path is entirely filled except when it overlaps "
"with a sub-path of a different direction that 'decrease the level of "
"overlapping' so that the region between the two is considered outside the "
"path and remain unfilled, like this:"
msgstr ""
"El botó amb el quadrat interior emplenat alterna el mode no zero, on la "
"major part del temps un camí que se superposa s'emplena completament, "
"excepte quan se superposa amb un camí secundari d'una direcció diferent que "
"«fa disminuir el nivell de la superposició», de manera que la regió entre "
"els dos es considerarà fora del camí i romandre sense emplenar, d'aquesta "
"manera:"

# skip-rule: t-acc_obe
#: ../../reference_manual/layers_and_masks/vector_layers.rst:148
msgid ""
"For more (and better) information about fill rules check the `Inkscape "
"manual <http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Attributes-Fill-Stroke."
"html#Attributes-Fill-Rule>`_."
msgstr ""
"Per obtenir més informació (i millor) sobre les regles de l'emplenat, "
"consulteu el `manual de l'Inkscape <http://tavmjong.free.fr/INKSCAPE/MANUAL/"
"html/Attributes-Fill-Stroke.html#Attributes-Fill-Rule>`_."
