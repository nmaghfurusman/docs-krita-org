msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dockers___touch_docker.pot\n"

#: ../../reference_manual/dockers/touch_docker.rst:1
msgid "Overview of the touch docker."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Touch"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Finger"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Tablet UI"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:15
msgid "Touch Docker"
msgstr "触控面板"

#: ../../reference_manual/dockers/touch_docker.rst:17
msgid ""
"The Touch Docker is a QML docker with several convenient actions on it. Its "
"purpose is to aid those who use Krita on a touch-enabled screen by providing "
"bigger gui elements."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:19
msgid "Its actions are..."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Open File"
msgstr "打开文件"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save File"
msgstr "保存文件"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save As"
msgstr "另存为"

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Undo"
msgstr "悔棋"

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Redo"
msgstr "重下"

#: ../../reference_manual/dockers/touch_docker.rst:26
msgid "Decrease Opacity"
msgstr "降低不透明度"

#: ../../reference_manual/dockers/touch_docker.rst:28
msgid "Increase Opacity"
msgstr "提高不透明度"

#: ../../reference_manual/dockers/touch_docker.rst:30
msgid "Increase Lightness"
msgstr "增加光度"

#: ../../reference_manual/dockers/touch_docker.rst:32
msgid "Decrease Lightness"
msgstr "减少光度"

#: ../../reference_manual/dockers/touch_docker.rst:34
msgid "Zoom in"
msgstr "放大"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Counter Clockwise 15°"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Reset Canvas Rotation"
msgstr "复位画布旋转"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Clockwise 15°"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:38
msgid "Zoom out"
msgstr "缩小"

#: ../../reference_manual/dockers/touch_docker.rst:40
msgid "Decrease Brush Size"
msgstr "降低笔刷尺寸"

#: ../../reference_manual/dockers/touch_docker.rst:42
msgid "Increase Brush Size"
msgstr "增加笔刷尺寸"

#: ../../reference_manual/dockers/touch_docker.rst:44
msgid "Delete Layer Contents"
msgstr ""
